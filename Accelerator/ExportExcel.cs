﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace Accelerator
{
    class ExportExcel
    {
        int rowNumber = 3;

        Excel.Application application;

        Excel.Workbook workbook;

        Excel.Worksheet worksheet;

        List<ProjectsCount> projectCount = new List<ProjectsCount>();

        List<ProjectType> projectType = new List<ProjectType>();

        string solutionPath, destPath;

        public ExportExcel(List<ProjectsCount> projectCount, List<ProjectType> projectType,string solutionPath, string destPath)
        {
            try
            {
                this.projectCount = projectCount;
                this.projectType = projectType;
                this.solutionPath = solutionPath;
                this.destPath = destPath;
                GenerateFile(this.projectCount, this.projectType, this.solutionPath, this.destPath);
            }
            catch (Exception)
            {
                GenerateFile(projectCount, projectType, solutionPath, destPath);
            }
        }

        private void GenerateFile(List<ProjectsCount> projectCount, List<ProjectType> projectType, string solutionPath, string destPath)
        {
            try
            {
                application = new Excel.Application();

                application.Visible = false;

                workbook = application.Workbooks.Add();

                worksheet = (Excel.Worksheet)workbook.ActiveSheet;

                worksheet.Name = "Projects";

                //worksheet.EnableSelection = Microsoft.Office.Interop.Excel.XlEnableSelection.xlNoSelection;

                Excel.Range range = worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[1, 10]];

                range.Merge();

                range = worksheet.Range[worksheet.Cells[1, 1], worksheet.Cells[1, 1]];

                range.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);

                range.Font.Bold = true;

                worksheet.Cells[1, 1] = "Solution: " + Path.GetFileName(solutionPath);



                range = worksheet.Range[worksheet.Cells[rowNumber, 1], worksheet.Cells[rowNumber, 3]];
                range.Merge();
                worksheet.Cells[rowNumber, 1] = "Project Type";
                worksheet.Cells[rowNumber, 4] = "Count";
                range = worksheet.Range[worksheet.Cells[rowNumber, 1], worksheet.Cells[rowNumber, 4]];
                range.Font.Bold = true;
                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.CornflowerBlue);
                rowNumber++;


                foreach (var item in projectCount)
                {
                    range = worksheet.Range[worksheet.Cells[rowNumber, 1], worksheet.Cells[rowNumber, 3]];
                    range.Merge();
                    worksheet.Cells[rowNumber, 1] = item.projectName;
                    worksheet.Cells[rowNumber, 4] = item.projectCount;
                    rowNumber++;
                }

                rowNumber += 2;

                range = worksheet.Range[worksheet.Cells[rowNumber, 1], worksheet.Cells[rowNumber, 4]];
                range.Merge();
                range.Font.Bold = true;
                range = worksheet.Range[worksheet.Cells[rowNumber, 5], worksheet.Cells[rowNumber, 8]];
                range.Merge();
                range.Font.Bold = true;
                range = worksheet.Range[worksheet.Cells[rowNumber, 9], worksheet.Cells[rowNumber, 12]];
                range.Merge();
                range.Font.Bold = true;
                range = worksheet.Range[worksheet.Cells[rowNumber, 1], worksheet.Cells[rowNumber, 12]];
                range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.CornflowerBlue);

                worksheet.Cells[rowNumber, 1] = "Project Name";
                worksheet.Cells[rowNumber, 5] = "Project Type";
                worksheet.Cells[rowNumber, 9] = "Cloud Solution";
                rowNumber++;


                foreach (var item in projectType)
                {
                    range = worksheet.Range[worksheet.Cells[rowNumber, 1], worksheet.Cells[rowNumber, 4]];
                    range.Merge();
                    worksheet.Cells[rowNumber, 1] = item.projectName;

                    range = worksheet.Range[worksheet.Cells[rowNumber, 5], worksheet.Cells[rowNumber, 8]];
                    range.Merge();
                    worksheet.Cells[rowNumber, 5] = item.projectType;

                    range = worksheet.Range[worksheet.Cells[rowNumber, 9], worksheet.Cells[rowNumber, 12]];
                    range.Merge();
                    worksheet.Cells[rowNumber, 9] = item.cloudService;

                    rowNumber++;
                }


                worksheet.Cells.Font.Size = 13;

                workbook.SaveAs(destPath + "\\" + Path.GetFileName(solutionPath) + ".xls");

                workbook.Close();

                application.Quit();

                Marshal.ReleaseComObject(worksheet);
                Marshal.ReleaseComObject(workbook);
                Marshal.ReleaseComObject(application);

            }
            catch (Exception)
            {

            }
        }

        public ExportExcel()
        {
        }
    }
}
