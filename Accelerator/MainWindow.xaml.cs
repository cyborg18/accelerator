﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Xml;

namespace Accelerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private BackgroundWorker myBackgroundWorker;

        static int webApiCount = 0, mvcCount = 0, totalProjectsCount = 0;

        //List for maintaining Project Name, Project Type and its respective cloud solution
        static List<ProjectType> projectsList = new List<ProjectType>();

        //List for maintaining Project Type and its Count.
        static List<ProjectsCount> projectsCount = new List<ProjectsCount>();

        List<string> webApiProjectList = new List<string>();
        List<string> mvcProjectList = new List<string>();

        static Dictionary<string, string> cloudMappings = new Dictionary<string, string>();
        static XmlDocument mappingsXMLDoc = new XmlDocument();
       
        static Dictionary<string, string> projectTypeIdentifiers = new Dictionary<string, string>();
        static XmlDocument projectTypeXMLDoc = new XmlDocument();

        string solutionPath = "";

        public MainWindow()
        {
            InitializeComponent();
            myBackgroundWorker = new BackgroundWorker();
            myBackgroundWorker.DoWork += MyBackgroundWorker_DoWork;
            myBackgroundWorker.ProgressChanged += MyBackgroundWorker_ProgressChanged;
            myBackgroundWorker.RunWorkerCompleted += MyBackgroundWorker_RunWorkerCompleted;
        }

        private void MyBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            countPanel.Visibility = Visibility.Visible;

            countPanel.ItemsSource = projectsCount;

            solnList.Visibility = Visibility.Visible;

            solnList.ItemsSource = projectsList;

            exportStack.Visibility = Visibility.Visible;
        }

        private void export_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();

            DialogResult result = folderDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    ExportExcel excel = new ExportExcel(projectsCount, projectsList, solutionPath, folderDialog.SelectedPath);

                }
                catch (Exception)
                {
                    ExportExcel excel = new ExportExcel(projectsCount, projectsList, solutionPath, folderDialog.SelectedPath);
                    System.Windows.MessageBox.Show("Created sucessfully");
                }
            }
        }

        private void MyBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void MyBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Core Logic Entry


            //load xml key, values into dictionary
            mappingsXMLDoc.Load(@"CloudMappings.xml");
            foreach (XmlNode xmlNode in mappingsXMLDoc.DocumentElement.ChildNodes)
            {
                if (!cloudMappings.ContainsKey(xmlNode.Attributes["name"].Value))
                    cloudMappings.Add(xmlNode.Attributes["name"].Value, xmlNode.Attributes["value"].Value);
            }

            projectTypeXMLDoc.Load(@"ProjectTypeIdentifiers.xml");
            foreach (XmlNode xmlNode in projectTypeXMLDoc.DocumentElement.ChildNodes)
            {
                if (!projectTypeIdentifiers.ContainsKey(xmlNode.Attributes["name"].Value))
                    projectTypeIdentifiers.Add(xmlNode.Attributes["name"].Value, xmlNode.Attributes["value"].Value);
            }

            this.Dispatcher.Invoke(() =>
            solutionPath = folderPath.Text
            );

            ProcessDirectory(solutionPath);

            int totalWebAppProjects = mvcCount + webApiCount;
        }


        //process the solution path to get all the projects in it
        private void ProcessDirectory(string targetDirectory)
        {
            try
            {
                string projectPath = "", projectName = "";
                List<string> dirs = new List<string>(Directory.EnumerateDirectories(targetDirectory));
                foreach (var dir in dirs)
                {

                    //Append project folder name to solution path.
                    projectPath = targetDirectory + "\\" + dir.Substring(dir.LastIndexOf(@"\") + 1);

                    projectName = dir.Substring(dir.LastIndexOf(@"\") + 1);

                    ProcessFile(projectPath, projectName);

                    projectPath = "";

                }
            }
            catch (Exception)
            {

            }
        }

        //classifies the project type
        private void ProcessFile(string projectPath, string projectName)
        {
            string targetPattern1 = projectName + ".csproj.user";

            //returns full file path if targetPattern match is found.
            string fullPath1 = Directory
                             .EnumerateFiles(projectPath, targetPattern1, SearchOption.AllDirectories)
                             .FirstOrDefault();

            if (fullPath1 == null)
            {
                targetPattern1 = projectName + ".csproj";
                fullPath1 = Directory
                             .EnumerateFiles(projectPath, targetPattern1, SearchOption.AllDirectories)
                             .FirstOrDefault();
            }

            //Console.WriteLine(fullPath1);

            if (fullPath1 != null)
            {
                string line = "", projectType = "";


                //read the csproj file for Guids
                using (StreamReader file = new StreamReader(fullPath1))
                {
                    while ((line = file.ReadLine()) != null)
                    {                        
                        foreach (KeyValuePair<string, string> entry in projectTypeIdentifiers)
                        {
                            if (line.Contains(entry.Key))
                                projectType = entry.Value;
                        }                      
                    }

                    //add project to list
                    if (projectType != "")
                    {
                        totalProjectsCount++;
                        projectsList.Add(new ProjectType()
                        {
                            projectName = projectName,
                            projectType = projectType,
                            cloudService = getCloudServiceType(projectType)
                        });
                        projectCountHelper(projectType);
                        //Console.WriteLine(projectName + " is a " + projectType);
                    }
                }
            }
        }


        //function to check if a web app is MVC or Web Api
        private void processWebApplication(string projectPath, string projectName)
        {
            string line = "";

            string targetPattern1 = "Global.asax.cs";

            //returns full file path if targetPattern match is found.
            string fullPath1 = Directory
                             .EnumerateFiles(projectPath, targetPattern1, SearchOption.AllDirectories)
                             .FirstOrDefault();

            if (File.Exists(fullPath1))
            {
                using (StreamReader file = new StreamReader(fullPath1))
                {
                    while ((line = file.ReadLine()) != null)
                    {

                        //check if the project is WebApi.
                        if (line.Contains("GlobalConfiguration.Configure(WebApiConfig.Register);"))
                        {
                            webApiCount++;
                            webApiProjectList.Add(projectName);
                        }

                        //check if the project is MVC.
                        if (line.Contains("RouteConfig.RegisterRoutes(RouteTable.Routes);"))
                        {
                            mvcCount++;
                            mvcProjectList.Add(projectName);
                        }
                    }
                }
            }
        }


        //Get cloud service mapping based on project type
        public static string getCloudServiceType(string projectType)
        {
            if (cloudMappings.ContainsKey(projectType))
                return cloudMappings[projectType];
            return "No Cloud Solution";
        }


        //Maintain the count of the projects in a list
        public static void projectCountHelper(string projectType)
        {
            var matchingProject = projectsCount.Where(project => project.projectType == projectType).FirstOrDefault();
            if (matchingProject == null)
            {
                projectsCount.Add(new ProjectsCount()
                {
                    projectType = projectType,
                    projectCount = 1
                });
            }
            else
            {
                matchingProject.projectCount += 1;
            }
        }


        private void browseFolder_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();

            DialogResult result = folderDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                folderPath.Text = folderDialog.SelectedPath;
                startProcess.IsEnabled = true;
            }
        }


        private void startProcess_Click(object sender, RoutedEventArgs e)
        {
            reset();
            myBackgroundWorker.RunWorkerAsync();
        }
        

        public static void reset()
        {
            projectsList = new List<ProjectType>();
            projectsCount = new List<ProjectsCount>();
        }
    }
}
