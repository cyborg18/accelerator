﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerator
{
    class ProjectType
    {
        public string projectName { get; set; }
        public string projectType { get; set; }
        public string cloudService { get; set; }
    }
}
